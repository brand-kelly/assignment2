from flask import Flask, render_template, request, redirect, jsonify, g
from flask_wtf import FlaskForm
from wtforms import StringField
import random

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

id_count = {'id': 3}

class MyForm(FlaskForm):
    name = StringField('name')

@app.route('/book/JSON')
def bookJSON():
    # <your code>
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
    if request.method == 'POST':
        if request.form['name'] not in books:
            id_count['id'] += 1
            books.append({'title': request.form['name'], 'id': f'{id_count["id"]}'})
        return redirect('/')
    return render_template('newBook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    # <your code>
    edit_book = {}
    for book in books:
        if book['id'] == str(book_id):
            edit_book = book
    if request.method == 'POST':
        edit_book['title'] = request.form['name']
        return redirect('/')
    return render_template('editBook.html', book = edit_book)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # <your code>
    book_delete = {}
    for book in books:
        if book['id'] == str(book_id):
            book_delete = book
    if request.method == 'POST':
        books.remove(book_delete)
        return redirect('/')
    return render_template('deleteBook.html', book=book_delete)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
